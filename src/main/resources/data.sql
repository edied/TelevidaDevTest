insert into TypeCall(name,active,version) values ('VOZ',true,0),('SMS',true,0),('APP',true,0);
insert into Currency(name,code,active,version) values ('QUETZAL','QTZ',true,0),('DOLAR','USD',true,0),('EURO','EUR',true,0);
insert into Country(name,code,currency_id,cost,active,version) values ('GUATEMALA','GT',(select id from currency where code='QTZ'),0.65,true,0);
insert into Country(name,code,currency_id,cost,active,version) values ('EL SALVADOR','SV',(select id from currency where code='USD'),0.2,true,0);
insert into CurrencyConversion(from_id,to_id,val,active,version) values ((select id from currency where code='QTZ'),(select id from currency where code='USD'),0.13016,true,0);
insert into CurrencyConversion(from_id,to_id,val,active,version) values ((select id from currency where code='USD'),(select id from currency where code='QTZ'),7.68290,true,0);

insert into Phone(active,phone,country_id,version) values
(true,'56988275',(select id from country where code='GT'),0),
(true,'56988276',(select id from country where code='SV'),0),
(true,'56988277',(select id from country where code='GT'),0),
(true,'56988278',(select id from country where code='SV'),0),
(true,'56988279',(select id from country where code='GT'),0),
(true,'56988280',(select id from country where code='SV'),0);
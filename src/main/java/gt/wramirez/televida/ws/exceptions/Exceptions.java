/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.exceptions;

/**
 *
 * @author Wilmer Ramirez (edied.ramirez@hotmail.com)
 */
public enum Exceptions {
   
    NoEncontrado("406","No Aceptado","Esto sucede cuando el registro no ha sido encontrado."),
    NoAceptado("406","No Aceptado","Al parecer uno de los datos enviados es incorrecto."),
    ValorNull("406","No Aceptado","Al parecer uno de los datos enviados es nulo."),
    ValorIncorrecto("406","No Aceptado","El Valor enviado es incorrecto."),
    ValorDuplicado("406","No Aceptado","El Codigo enviado ya fue procesado."),
    ConversionDivisaNoEncontrada("406","No Aceptado","Esto sucede cuando no se encuentra la conversion de divisas.");
    
    
    private final String code;
    private final String name;
    private final String mensaje;

    
    
    Exceptions(String code,String name,String mensaje){
        this.name=name;
        this.code=code;
        this.mensaje=mensaje;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }
}

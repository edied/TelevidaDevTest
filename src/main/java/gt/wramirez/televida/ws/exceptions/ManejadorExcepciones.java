/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.exceptions;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Steve Ortiz
 */
@ControllerAdvice
public class ManejadorExcepciones {

    public static org.slf4j.Logger log = LoggerFactory.getLogger(ManejadorExcepciones.class);

    @ResponseBody
    @ExceptionHandler(InfoException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    Map<String, String> entidadInternalServerErrorExceptionHandler(InfoException ex) {
        Map<String, String> res = new HashMap<>();
        res.put("titulo", "Uppss");
        res.put("error", "Hemos encontrado un inconveniente.");
        res.put("mensaje", ex.getMessage());
        return res;
    }

}

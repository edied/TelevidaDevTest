/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.exceptions;

/**
 *
 * @author Steve Ortiz 
 */
public class InfoException extends Exception {

    private static final long serialVersionUID = -7539901274547128652L;

    public InfoException() {
        super();
    }

    public InfoException(final String Mensaje) {
        super(Mensaje);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.repo;

import gt.wramirez.televida.entities.PhoneRecharge;
import java.io.Serializable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Wilmer Ramirez (edied.ramirez@hotmail.com)
 */
@Repository
public interface PhoneRechargeRepo extends PagingAndSortingRepository<PhoneRecharge, Long> {

    PhoneRecharge findByCodeEquals(String code);
}

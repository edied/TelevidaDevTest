/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.repo;

import gt.wramirez.televida.entities.Currency;
import gt.wramirez.televida.entities.CurrencyConversion;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Wilmer Ramirez (edied.ramirez@hotmail.com)
 */
@Repository
public interface CurrencyConversionRepo extends PagingAndSortingRepository<CurrencyConversion, Long>{
    CurrencyConversion findByFromEqualsAndToEquals(Currency from,Currency to);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.repo;


import gt.wramirez.televida.entities.Phone;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author wilmer.ramirez
 */
@Repository
public interface PhoneRepo extends PagingAndSortingRepository<Phone, Long> {

    Phone findByPhoneEquals(String phone);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.inte;

import gt.wramirez.televida.entities.TypeCall;
import gt.wramirez.televida.ws.exceptions.InfoException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wilmer.ramirez
 */
@RestController
@RequestMapping(value = "/api/TypeCall",
        produces = {MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE})
public interface TypeCallInt {

    @Transactional(readOnly = false)
    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    ResponseEntity<Iterable<TypeCall>> doFindAllByActive(HttpServletRequest request, HttpServletResponse response) throws InfoException;

}

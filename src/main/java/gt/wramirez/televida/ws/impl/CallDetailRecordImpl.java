/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.impl;

import gt.wramirez.televida.entities.CallDetailRecord;
import gt.wramirez.televida.entities.CurrencyConversion;
import gt.wramirez.televida.entities.Phone;
import gt.wramirez.televida.entities.TypeCall;
import gt.wramirez.televida.ws.exceptions.Exceptions;
import gt.wramirez.televida.ws.exceptions.InfoException;
import static gt.wramirez.televida.ws.impl.PhoneRechargeImpl.log;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import gt.wramirez.televida.ws.inte.CallDetailRecordInt;
import gt.wramirez.televida.ws.repo.CallDetailRecordRepo;
import gt.wramirez.televida.ws.repo.CurrencyConversionRepo;
import gt.wramirez.televida.ws.repo.CurrencyRepo;
import gt.wramirez.televida.ws.repo.PhoneRechargeRepo;
import gt.wramirez.televida.ws.repo.PhoneRepo;
import gt.wramirez.televida.ws.repo.TypeCallRepo;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

/**
 *
 * @author wilmer.ramirez
 */
@Component
public class CallDetailRecordImpl implements CallDetailRecordInt {

    public static org.slf4j.Logger log = LoggerFactory.getLogger(CallDetailRecordImpl.class);

    @Autowired
    private CallDetailRecordRepo repository;
    @Autowired
    private PhoneRepo phoneRepo;
    @Autowired
    private CurrencyRepo currencyRepo;
    @Autowired
    private CurrencyConversionRepo currencyConversionRepo;
    @Autowired
    private TypeCallRepo typeCallRepo;

    @Override
    public ResponseEntity<CallDetailRecord> doCreate(CallDetailRecord callDetailRecord, HttpServletRequest request, HttpServletResponse response) throws InfoException {
        log.info("callDetailRecord {}", callDetailRecord);
        if(callDetailRecord==null || callDetailRecord.getTypeCall()==null){
            throw new InfoException(String.format("%s %s", Exceptions.ValorNull.getMensaje(), callDetailRecord));
        }
        Phone from = phoneRepo.findOne(callDetailRecord.getFrom().getId());
        Phone to = phoneRepo.findOne(callDetailRecord.getTo().getId());
        Double conversionValue = 1.0;
        if (!Objects.equals(from.getCountry().getId(), to.getCountry().getId())) {
            CurrencyConversion currencyConversion = currencyConversionRepo.findByFromEqualsAndToEquals(to.getCountry().getCurrency(), from.getCountry().getCurrency());
            if (currencyConversion == null) {
                throw new InfoException(String.format("%s %s <> %s", Exceptions.ConversionDivisaNoEncontrada.getMensaje(), from.getCountry().getCurrency().getCode(), to.getCountry().getCurrency().getCode()));
            }
            conversionValue = currencyConversion.getVal();
        }
        TypeCall typeCall = typeCallRepo.findOne(callDetailRecord.getTypeCall().getId());
        Double cost = (callDetailRecord.getSeconds() / 60) * from.getCountry().getCost() * conversionValue;
        log.info("cost {}", cost);
        CallDetailRecord callDetailRecordNew = new CallDetailRecord(from, to, callDetailRecord.getStartDate(), callDetailRecord.getEndDate(), callDetailRecord.getSeconds(), typeCall);
        callDetailRecordNew.setCost(cost);
        repository.save(callDetailRecordNew);
        Double available = (from.getMoneyAvailable() == null ? 0.00 : from.getMoneyAvailable());
        log.info("available1 {}", available);
        available = available - cost;
        log.info("available2 {}", available);
        from.setMoneyAvailable(available);
        phoneRepo.save(from);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}

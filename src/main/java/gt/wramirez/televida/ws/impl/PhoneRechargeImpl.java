/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.impl;

import gt.wramirez.televida.entities.Currency;
import gt.wramirez.televida.entities.CurrencyConversion;
import gt.wramirez.televida.entities.Phone;
import gt.wramirez.televida.entities.PhoneRecharge;
import gt.wramirez.televida.ws.exceptions.Exceptions;
import gt.wramirez.televida.ws.exceptions.InfoException;
import gt.wramirez.televida.ws.inte.PhoneRechargeInt;
import gt.wramirez.televida.ws.repo.CurrencyConversionRepo;
import gt.wramirez.televida.ws.repo.CurrencyRepo;
import gt.wramirez.televida.ws.repo.PhoneRechargeRepo;
import gt.wramirez.televida.ws.repo.PhoneRepo;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 *
 * @author wilmer.ramirez
 */
@Component
public class PhoneRechargeImpl implements PhoneRechargeInt {

    public static org.slf4j.Logger log = LoggerFactory.getLogger(PhoneRechargeImpl.class);

    @Autowired
    private PhoneRechargeRepo repository;
    @Autowired
    private PhoneRepo phoneRepo;
    @Autowired
    private CurrencyRepo currencyRepo;
    @Autowired
    private CurrencyConversionRepo currencyConversionRepo;

    @Override
    public ResponseEntity<PhoneRecharge> doCreate(PhoneRecharge phoneRecharge, HttpServletRequest request, HttpServletResponse response) throws InfoException {
        log.debug("phoneRecharge {} - {}", phoneRecharge.getPhone(), phoneRecharge);
        if (phoneRecharge.getCode() == null || phoneRecharge.getPhone() == null || phoneRecharge.getPhone().getId() == null) {
            throw new InfoException(String.format("%s %s", Exceptions.ValorNull.getMensaje(), phoneRecharge));
        }
        if (phoneRecharge.getVal() <= 0) {
            throw new InfoException(String.format("%s %s Debe ser mayor a 0.0", Exceptions.ValorIncorrecto.getMensaje(), phoneRecharge.getVal()));
        }
        Phone phone = phoneRepo.findOne(phoneRecharge.getPhone().getId());
        if (phone == null) {
            throw new InfoException(String.format("%s Phone", Exceptions.NoEncontrado.getMensaje()));
        }
        log.debug("phone {} - {}", phoneRecharge.getPhone(), phone);
        if (phoneRecharge.getCurrency() == null) {
            throw new InfoException(String.format("%s Currency", Exceptions.ValorNull.getMensaje()));
        }
        Currency currency = currencyRepo.findOne(phoneRecharge.getCurrency().getId());
        if (currency == null) {
            throw new InfoException(String.format("%s Currency", Exceptions.NoEncontrado.getMensaje()));
        }
        Double conversionValue = 1.0;
        if (!Objects.equals(phone.getCountry().getCurrency().getId(), currency.getId())) {
            CurrencyConversion currencyConversion = currencyConversionRepo.findByFromEqualsAndToEquals(currency, phone.getCountry().getCurrency());
            if (currencyConversion == null) {
                throw new InfoException(String.format("%s %s <> %s", Exceptions.ConversionDivisaNoEncontrada.getMensaje(), phone.getCountry().getCurrency().getCode(), currency.getCode()));
            }
            conversionValue = currencyConversion.getVal();
        }
        log.debug("currency {} - {}", phoneRecharge.getPhone(), currency);
        PhoneRecharge newRecharge = repository.findByCodeEquals(phoneRecharge.getCode());
        if (newRecharge != null) {
            log.info("newRecharge 01 {} - {}", phoneRecharge.getPhone(), newRecharge);
            throw new InfoException(String.format("%s %s ya fue procesado.", Exceptions.ValorDuplicado.getMensaje(), phoneRecharge.getCode()));
        }
        newRecharge = new PhoneRecharge(phoneRecharge.getCode(), phone, currency, phoneRecharge.getVal());
        repository.save(newRecharge);
        log.debug("newRecharge 02 {} - {}", phoneRecharge.getPhone(), newRecharge);
        Double available = (phone.getMoneyAvailable() == null ? 0.00 : phone.getMoneyAvailable()) + (phoneRecharge.getVal() * conversionValue);
        phone.setMoneyAvailable(available);
        log.debug("phone {} - {}", phoneRecharge.getPhone(), phone);
        phoneRepo.save(phone);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}

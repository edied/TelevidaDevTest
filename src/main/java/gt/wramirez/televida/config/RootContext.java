/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.config;

/**
 *
 * @author Steve Ortiz
 */
import com.zaxxer.hikari.HikariDataSource;
import gt.wramirez.televida.config.util.Constant;
import java.util.Properties;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * An application context Java configuration class. The usage of Java
 * configuration requires Spring Framework 3.0 or higher with following
 * exceptions:
 * <ul>
 * <li>@EnableWebMvc annotation requires Spring Framework 3.1</li>
 * </ul>
 *
 * @author Petri Kainulainen
 */
@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager", basePackages = {"gt.wramirez.televida"}
)
@EnableTransactionManagement
public class RootContext {

    private static final Logger log = LoggerFactory.getLogger(RootContext.class);

    @Resource
    private Environment environment;

    @Bean(name = "dataSource")
    @Qualifier("dataSource")
    @Primary
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        log.debug(dataSource.toString());
        dataSource.setDriverClassName(environment.getRequiredProperty(Constant.PROPERTY_NAME_DATABASE_DRIVER));
        dataSource.setJdbcUrl(environment.getRequiredProperty(Constant.PROPERTY_NAME_DATABASE_URL));
        dataSource.setUsername(environment.getRequiredProperty(Constant.PROPERTY_NAME_DATABASE_USERNAME));
        dataSource.setPassword(environment.getRequiredProperty(Constant.PROPERTY_NAME_DATABASE_PASSWORD));
        dataSource.setMaximumPoolSize(Integer.parseInt(environment.getRequiredProperty(Constant.PROPERTY_NAME_DATA_SOURCER_maximunPoolSize)));
        return dataSource;
    }

    
    @Bean(name = "entityManager")
    @Qualifier(value = "entityManager")
    public EntityManager entityManager() throws ClassNotFoundException {
        return entityManagerFactory().createEntityManager();
    }

    @Bean(name = "entityManagerFactory")
    @Qualifier("entityManagerFactory")
    public EntityManagerFactory entityManagerFactory() throws ClassNotFoundException {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        log.debug("urlToScan1 [{}]", environment.getRequiredProperty(Constant.PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));
        entityManagerFactoryBean.setPackagesToScan(environment.getRequiredProperty(Constant.PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        Properties jpaProterties = new Properties();
        jpaProterties.put("hibernate.dialect", environment.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_DIALECT));
        jpaProterties.put("hibernate.format_sql", environment.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_FORMAT_SQL));
        jpaProterties.put("hibernate.show_sql", environment.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_SHOW_SQL));
        jpaProterties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty(Constant.PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO));
        
        entityManagerFactoryBean.setJpaProperties(jpaProterties);
        entityManagerFactoryBean.afterPropertiesSet();
        log.debug(entityManagerFactoryBean.toString());
        return entityManagerFactoryBean.getObject();
    }

    @Bean(name = "transactionManager")
    @Qualifier("transactionManager")
    @Primary
    public PlatformTransactionManager transactionManager() throws ClassNotFoundException {
        return new JpaTransactionManager(entityManagerFactory());
    }

    /**
     *
     * @return
     */
    @Bean
    public JpaDialect jpaDialect() {
        return new HibernateJpaDialect();
    }

    /**
     *
     * @return
     */
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix(Constant.VIEW_RESOLVER_PREFIX);
        viewResolver.setSuffix(Constant.VIEW_RESOLVER_SUFFIX);
        log.debug(viewResolver.toString());
        return viewResolver;
    }

    /**
     *
     * @return
     */
    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

}

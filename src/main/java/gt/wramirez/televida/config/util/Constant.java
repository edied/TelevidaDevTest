/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.config.util;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author wilmer.ramirez
 * @version 1.2 23 de Junio de 2016
 */
public class Constant {

    /**
     * Variable publica: Contiene el prefijo para resolver las vistas
     */
    public static final String VIEW_RESOLVER_PREFIX = "/WEB-INF/views/";

    /**
     *
     */
    public static final String VIEW_RESOLVER_SUFFIX = ".html";

    /**
     *
     */
    public static final String PROPERTY_NAME_DATABASE_DRIVER = "gt.wramirez.televida.db.driver";

    /**
     *
     */
    public static final String PROPERTY_NAME_DATABASE_PASSWORD = "gt.wramirez.televida.db.password";

    /**
     *
     */
    public static final String PROPERTY_NAME_DATABASE_URL = "gt.wramirez.televida.db.url";

    /**
     *
     */
    public static final String PROPERTY_NAME_DATABASE_USERNAME = "gt.wramirez.televida.db.username";

    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_DIALECT = "gt.wramirez.televida.hibernate.dialect";

    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_FORMAT_SQL = "gt.wramirez.televida.hibernate.format_sql";

    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "gt.wramirez.televida.hibernate.show_sql";

    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO = "gt.wramirez.televida.hibernate.hbm2ddl.auto";

    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_default_catalog = "gt.wramirez.televida.hibernate.default_catalog";

    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_default_schema = "gt.wramirez.televida.hibernate.default_schema";

    /**
     *
     */
    public static final String PROPERTY_NAME_DATA_SOURCER_maximunPoolSize = "gt.wramirez.televida.datasource.maximunPoolSize";

    /* ENVERS AUDITABLE */
    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_AUDITABLE = "gt.wramirez.televida.hibernate.auditable";

    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_ENVERS_default_schema = "gt.wramirez.televida.hibernate.envers.default_schema";

    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_ENVERS_default_catalog = "gt.wramirez.televida.hibernate.envers.default_catalog";

    /**
     *
     */
    public static final String PROPERTY_NAME_HIBERNATE_ENVERS_storeDataAtDelete = "gt.wramirez.televida.hibernate.envers.storeDataAtDelete";

    /**
     *
     */
    public static final String PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN = "gt.wramirez.televida.entitymanager.packages.to.scan";

    /**
     *
     */
    public static final String EXP_REG_PARAMETERS_SEARCH = "(\\w+?)(BTW|IN|EQ|!IN|LK|!LK|GT|GOE|LT|LOE|!)(\\w+(,\\w+)+)+\\|";

    /**
     *
     */
    public static final String EXP_REG_PARAMETERS_SEARCH_ORDER = "(\\w+?)(:)(ASC|DESC)\\|";

    public static Date sumarRestarFecha(Date fecha, int tipo, int valor) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(tipo, valor); // valor a añadir o restar 
        return calendar.getTime(); // Devuelve el objeto Date 
    }

}

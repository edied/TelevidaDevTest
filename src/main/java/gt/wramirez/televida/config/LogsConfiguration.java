/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.config;


import gt.wramirez.televida.InformacionAplicacion;
import org.slf4j.MDC;

/**
 *
 * @author Steve Ortiz
 */
public class LogsConfiguration {

    /**
     *
     */
    public final static String key = "aplicacion";

    private static String Discriminador() {
        return new StringBuilder().append(InformacionAplicacion.getOrganizacion()).
                append("/").
                append(InformacionAplicacion.getNombre()).
                append("/").
                append(InformacionAplicacion.getVersion()).toString();
    }

    /**
     *
     */
    public static void addKey() {
        MDC.put(key, Discriminador());
    }

    /**
     *
     */
    public static void removeKey() {
        MDC.remove(key);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.entities;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Wilmer Ramirez (edied.ramirez@hotmail.com)
 */
@Entity
public class PhoneRecharge extends GenericEntity {

    @Column(nullable = false, unique = true)
    private String code;
    @ManyToOne
    private Phone phone;
    @ManyToOne(optional = false)
    private Currency currency;
    private Double val;

    public PhoneRecharge() {
    }

    public PhoneRecharge(String code, Phone phone, Currency currency, Double val) {
        this.code = code;
        this.phone = phone;
        this.currency = currency;
        this.val = val;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Double getVal() {
        return val;
    }

    public void setVal(Double val) {
        this.val = val;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.code);
        hash = 89 * hash + Objects.hashCode(this.phone);
        hash = 89 * hash + Objects.hashCode(this.currency);
        hash = 89 * hash + Objects.hashCode(this.val);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PhoneRecharge other = (PhoneRecharge) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        if (!Objects.equals(this.val, other.val)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PhoneRecharge{" + "code=" + code + ", phone=" + phone + ", currency=" + currency + ", val=" + val + '}';
    }
    
    

}

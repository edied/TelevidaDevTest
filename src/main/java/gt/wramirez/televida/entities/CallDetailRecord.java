/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.entities;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Wilmer Ramirez (edied.ramirez@hotmail.com)
 */
@Entity
public class CallDetailRecord extends GenericEntity {

    @ManyToOne
    private Phone from;
    @ManyToOne
    private Phone to;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date startDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date endDate;
    private Integer seconds;
    private Double cost;
    @ManyToOne
    private TypeCall typeCall;

    public CallDetailRecord() {
    }

    public CallDetailRecord(Phone from, Phone to, Date startDate, Date endDate,Integer seconds, TypeCall typeCall) {
        this.from = from;
        this.to = to;
        this.startDate = startDate;
        this.endDate = endDate;
        this.seconds=seconds;
        this.typeCall = typeCall;
    }

    public Phone getFrom() {
        return from;
    }

    public void setFrom(Phone from) {
        this.from = from;
    }

    public Phone getTo() {
        return to;
    }

    public void setTo(Phone to) {
        this.to = to;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getSeconds() {
        return seconds;
    }

    public void setSeconds(Integer seconds) {
        this.seconds = seconds;
    }

    public TypeCall getTypeCall() {
        return typeCall;
    }

    public void setTypeCall(TypeCall typeCall) {
        this.typeCall = typeCall;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.from);
        hash = 53 * hash + Objects.hashCode(this.to);
        hash = 53 * hash + Objects.hashCode(this.startDate);
        hash = 53 * hash + Objects.hashCode(this.endDate);
        hash = 53 * hash + Objects.hashCode(this.seconds);
        hash = 53 * hash + Objects.hashCode(this.cost);
        hash = 53 * hash + Objects.hashCode(this.typeCall);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CallDetailRecord other = (CallDetailRecord) obj;
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        if (!Objects.equals(this.seconds, other.seconds)) {
            return false;
        }
        if (!Objects.equals(this.cost, other.cost)) {
            return false;
        }
        if (!Objects.equals(this.typeCall, other.typeCall)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CallDetailRecord{" + "from=" + from + ", to=" + to + ", startDate=" + startDate + ", endDate=" + endDate + ", seconds=" + seconds + ", cost=" + cost + ", typeCall=" + typeCall + '}';
    }
    
    

}

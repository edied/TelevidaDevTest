/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.entities;

import gt.wramirez.televida.entities.Country;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Wilmer Ramirez (edied.ramirez@hotmail.com)
 */
@Entity
public class CurrencyConversion extends GenericEntity{
    
    @ManyToOne
    private Currency from;
    @ManyToOne
    private Currency to;
    private Double val;

    public CurrencyConversion() {
    }

    public CurrencyConversion(Currency from, Currency to, Double val) {
        this.from = from;
        this.to = to;
        this.val = val;
    }

    public Currency getFrom() {
        return from;
    }

    public void setFrom(Currency from) {
        this.from = from;
    }

    public Currency getTo() {
        return to;
    }

    public void setTo(Currency to) {
        this.to = to;
    }

    public Double getVal() {
        return val;
    }

    public void setVal(Double val) {
        this.val = val;
    }
    
    
    
}

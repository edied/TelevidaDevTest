/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.entities;

import gt.wramirez.televida.entities.Country;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Wilmer Ramirez (edied.ramirez@hotmail.com)
 */
@Entity
public class Phone extends GenericEntity {

    private String firstName;
    private String lastName;
    @Column(unique = true)
    private String phone;
    @ManyToOne
    private Country country;
    private Double moneyAvailable;

    public Phone() {
    }

    public Phone(String firstName, String lastName, String phone, Country country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.country = country;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Double getMoneyAvailable() {
        return moneyAvailable;
    }

    public void setMoneyAvailable(Double moneyAvailable) {
        this.moneyAvailable = moneyAvailable;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.firstName);
        hash = 53 * hash + Objects.hashCode(this.lastName);
        hash = 53 * hash + Objects.hashCode(this.phone);
        hash = 53 * hash + Objects.hashCode(this.country);
        hash = 53 * hash + Objects.hashCode(this.moneyAvailable);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Phone other = (Phone) obj;
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.moneyAvailable, other.moneyAvailable)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Phone{" + "firstName=" + firstName + ", lastName=" + lastName + ", phone=" + phone + ", country=" + country + ", moneyAvailable=" + moneyAvailable + '}';
    }
    
    

}

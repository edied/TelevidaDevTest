/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida;

import com.jayway.restassured.RestAssured;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;


/**
 *
 * @author wilmer.ramirez
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public abstract class IntegrationTestCase {

    public static org.slf4j.Logger log = LoggerFactory.getLogger(IntegrationTestCase.class);

    @Autowired
    protected WebApplicationContext webApplicationContext;

    RequestPostProcessor bearerToken = null;

    protected MockMvc mvc;

    @Value("${local.server.port}")
    private int serverPort;
    protected String accessToken = null;
    protected String clientBasicAuthCredentials;

    /**
     * Prepares the test class for execution of web tests. Builds a MockMvc
     * instance. Call this method from the concrete JUnit test class in the
     * <code>@Before</code> setup method.
     */
    @Before
    protected void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        RestAssured.port = serverPort;
    }
   

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.impl;

import static org.hamcrest.Matchers.*;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.http.ContentType;
import gt.wramirez.televida.IntegrationTestCase;
import gt.wramirez.televida.config.util.Constant;
import gt.wramirez.televida.entities.CallDetailRecord;
import gt.wramirez.televida.entities.Currency;
import gt.wramirez.televida.entities.Phone;
import gt.wramirez.televida.entities.PhoneRecharge;
import gt.wramirez.televida.ws.exceptions.Exceptions;
import gt.wramirez.televida.ws.repo.CountryRepo;
import gt.wramirez.televida.ws.repo.CurrencyRepo;
import gt.wramirez.televida.ws.repo.PhoneRechargeRepo;
import gt.wramirez.televida.ws.repo.PhoneRepo;
import gt.wramirez.televida.ws.repo.TypeCallRepo;
import java.util.Calendar;
import java.util.Date;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Wilmer Ramirez (edied.ramirez@hotmail.com)
 */
public class CallDetailRecordImplIT extends IntegrationTestCase {

    private static final String RESOURCE = "/api/CallDetailRecord/";
    private static final String MESSAGE_FIELD = "mensaje";
    private static final String PHONE_QTZ_1 = "56988275";
    private static final String PHONE_QTZ_2 = "56988277";
    private static final String PHONE_USD_1 = "56988276";
    private static final String PHONE_USD_2 = "56988278";

    private CallDetailRecord MODEL = null;

    @Autowired
    private PhoneRechargeRepo repository;
    @Autowired
    private PhoneRepo phoneRepo;
    @Autowired
    private TypeCallRepo typeCallRepo;
    @Autowired
    private CountryRepo countryRepo;

    @Before
    @Override
    public void setUp() {
        super.setUp();
    }

    @Test
    public void callToPhoneWithEqualsCountryShouldReturnCreatedCode() {
        Date inicio = new Date();
        Date fin = Constant.sumarRestarFecha(inicio, Calendar.MINUTE, 10);
        Integer seconds = new Long(((fin.getTime() - inicio.getTime()) / 1000)).intValue();
        MODEL = new CallDetailRecord(phoneRepo.findByPhoneEquals(PHONE_QTZ_1), phoneRepo.findByPhoneEquals(PHONE_QTZ_2), inicio, fin, seconds, typeCallRepo.findByNameEquals("VOZ"));
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.CREATED_201);
    }

    @Test
    public void callToPhoneWithDiferenteCountryShouldReturnCreatedCode() {
        Date inicio = new Date();
        Date fin = Constant.sumarRestarFecha(inicio, Calendar.MINUTE, 10);
        Integer seconds = new Long(((fin.getTime() - inicio.getTime()) / 1000)).intValue();
        MODEL = new CallDetailRecord(phoneRepo.findByPhoneEquals(PHONE_QTZ_1), phoneRepo.findByPhoneEquals(PHONE_USD_1), inicio, fin, seconds, typeCallRepo.findByNameEquals("VOZ"));
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.CREATED_201);
    }
    
    @Test
    public void smsToPhoneWithEqualsCountryShouldReturnCreatedCode() {
        Date inicio = new Date();
        Date fin = Constant.sumarRestarFecha(inicio, Calendar.MINUTE, 10);
        Integer seconds = new Long(((fin.getTime() - inicio.getTime()) / 1000)).intValue();
        MODEL = new CallDetailRecord(phoneRepo.findByPhoneEquals(PHONE_QTZ_1), phoneRepo.findByPhoneEquals(PHONE_QTZ_2), inicio, fin, seconds, typeCallRepo.findByNameEquals("SMS"));
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.CREATED_201);
    }

    @Test
    public void smsToPhoneWithDiferenteCountryShouldReturnCreatedCode() {
        Date inicio = new Date();
        Date fin = Constant.sumarRestarFecha(inicio, Calendar.MINUTE, 10);
        Integer seconds = new Long(((fin.getTime() - inicio.getTime()) / 1000)).intValue();
        MODEL = new CallDetailRecord(phoneRepo.findByPhoneEquals(PHONE_QTZ_1), phoneRepo.findByPhoneEquals(PHONE_USD_1), inicio, fin, seconds, typeCallRepo.findByNameEquals("SMS"));
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.CREATED_201);
    }
    
    @Test
    public void appToPhoneWithEqualsCountryShouldReturnCreatedCode() {
        Date inicio = new Date();
        Date fin = Constant.sumarRestarFecha(inicio, Calendar.MINUTE, 10);
        Integer seconds = new Long(((fin.getTime() - inicio.getTime()) / 1000)).intValue();
        MODEL = new CallDetailRecord(phoneRepo.findByPhoneEquals(PHONE_QTZ_1), phoneRepo.findByPhoneEquals(PHONE_QTZ_2), inicio, fin, seconds, typeCallRepo.findByNameEquals("APP"));
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.CREATED_201);
    }

    @Test
    public void appToPhoneWithDiferenteCountryShouldReturnCreatedCode() {
        Date inicio = new Date();
        Date fin = Constant.sumarRestarFecha(inicio, Calendar.MINUTE, 10);
        Integer seconds = new Long(((fin.getTime() - inicio.getTime()) / 1000)).intValue();
        MODEL = new CallDetailRecord(phoneRepo.findByPhoneEquals(PHONE_QTZ_1), phoneRepo.findByPhoneEquals(PHONE_USD_1), inicio, fin, seconds, typeCallRepo.findByNameEquals("APP"));
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.CREATED_201);
    }
    
    @Test
    public void callToPhoneWithOutTypeCallShouldReturnBadRequest() {
        Date inicio = new Date();
        Date fin = Constant.sumarRestarFecha(inicio, Calendar.MINUTE, 10);
        Integer seconds = new Long(((fin.getTime() - inicio.getTime()) / 1000)).intValue();
        MODEL = new CallDetailRecord(phoneRepo.findByPhoneEquals(PHONE_QTZ_1), phoneRepo.findByPhoneEquals(PHONE_QTZ_2), inicio, fin, seconds, typeCallRepo.findByNameEquals("VOZIP"));
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406)
                .body(MESSAGE_FIELD, containsString(Exceptions.ValorNull.getMensaje()));
    }

}

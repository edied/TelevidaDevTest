/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.wramirez.televida.ws.impl;

import static org.hamcrest.Matchers.*;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.http.ContentType;
import gt.wramirez.televida.IntegrationTestCase;
import gt.wramirez.televida.entities.Currency;
import gt.wramirez.televida.entities.Phone;
import gt.wramirez.televida.entities.PhoneRecharge;
import gt.wramirez.televida.ws.exceptions.Exceptions;
import gt.wramirez.televida.ws.repo.CountryRepo;
import gt.wramirez.televida.ws.repo.CurrencyRepo;
import gt.wramirez.televida.ws.repo.PhoneRechargeRepo;
import gt.wramirez.televida.ws.repo.PhoneRepo;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Wilmer Ramirez (edied.ramirez@hotmail.com)
 */
public class PhoneRechargeImplIT extends IntegrationTestCase {

    private static final String RESOURCE = "/api/PhoneRecharge/";
    private static final String MESSAGE_FIELD = "mensaje";
    private static final String CODE_QTZ = "123456";
    private static final String CODE_USD = "1234567";

    private PhoneRecharge MODEL = null;

    @Autowired
    private PhoneRechargeRepo repository;
    @Autowired
    private PhoneRepo phoneRepo;
    @Autowired
    private CurrencyRepo currencyRepo;
    @Autowired
    private CountryRepo countryRepo;

    @Before
    @Override
    public void setUp() {
        super.setUp();
    }

    @Test
    public void addPhoneRechargeShouldReturnCreatedCode() {
        repository.deleteAll();
        MODEL = new PhoneRecharge(CODE_QTZ, phoneRepo.findByPhoneEquals("56988275"), currencyRepo.findByCodeEquals("QTZ"), 5.0);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.CREATED_201);
    }
    
    @Test
    public void addPhoneRechargeWithDifferentCurrencyShouldReturnCreatedCode() {
        MODEL = new PhoneRecharge(CODE_USD, phoneRepo.findByPhoneEquals("56988275"), currencyRepo.findByCodeEquals("USD"), 1.0);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.CREATED_201);
    }
    
    @Test
    public void addPhoneRechargeWithConversionCurrencyNotExistShouldReturnCreatedCode() {
        MODEL = new PhoneRecharge(CODE_USD, phoneRepo.findByPhoneEquals("56988275"), currencyRepo.findByCodeEquals("EUR"), 1.0);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406)
                .body(MESSAGE_FIELD, containsString(Exceptions.ConversionDivisaNoEncontrada.getMensaje()));
    }

    @Test
    public void addPhoneRechargeWithRepeatCodeShouldReturnBadRequest() {
        MODEL = new PhoneRecharge(CODE_QTZ, phoneRepo.findByPhoneEquals("56988275"), currencyRepo.findByCodeEquals("QTZ"), 5.0);
        MODEL = repository.save(MODEL);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406)
                .body(MESSAGE_FIELD, containsString(Exceptions.ValorDuplicado.getMensaje()));
    }

    @Test
    public void addPhoneRechargeWihtOutCodeShouldReturnBadRequest() {
        MODEL = new PhoneRecharge(null, phoneRepo.findByPhoneEquals("56988277"), currencyRepo.findByCodeEquals("QTZ"), 1.0);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406)
                .body(MESSAGE_FIELD, containsString(Exceptions.ValorNull.getMensaje()));
    }

    @Test
    public void addPhoneRechargeWihtOutPhoneShouldReturnBadRequest() {
        MODEL = new PhoneRecharge(CODE_QTZ, null, currencyRepo.findByCodeEquals("QTZ"), 1.0);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406)
                .body(MESSAGE_FIELD, containsString(Exceptions.ValorNull.getMensaje()));
    }

    @Test
    public void addPhoneRechargeWihtOutCurrencyShouldReturnBadRequest() {
        MODEL = new PhoneRecharge(CODE_QTZ, phoneRepo.findByPhoneEquals("56988275"), null, 1.0);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406)
                .body(MESSAGE_FIELD, containsString(Exceptions.ValorNull.getMensaje()));
    }

    @Test
    public void addPhoneRechargeWihtPhoneInvalidOrNotExistShouldReturnBadRequest() {
        Phone phone = new Phone(null, null, "11111", countryRepo.findByCodeEquals("GT"));
        phone.setId(Long.MAX_VALUE);
        MODEL = new PhoneRecharge("-1", phone, currencyRepo.findByCodeEquals("QTZ"), 1.0);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406)
                .body(MESSAGE_FIELD, containsString(Exceptions.NoEncontrado.getMensaje()));
    }

    @Test
    public void addPhoneRechargeWihtCurrencyInvalidOrNotExistShouldReturnBadRequest() {
        Currency currency = new Currency("Not Exist", "ABC");
        currency.setId(Long.MAX_VALUE);
        MODEL = new PhoneRecharge("-2", phoneRepo.findByPhoneEquals("56988275"), currency, 1.0);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406)
                .body(MESSAGE_FIELD, containsString(Exceptions.NoEncontrado.getMensaje()));
    }

    @Test
    public void addPhoneRechargeWihtValue0ShouldReturnBadRequest() {
        MODEL = new PhoneRecharge("-3", phoneRepo.findByPhoneEquals("56988275"), currencyRepo.findByCodeEquals("QTZ"), 0.0);
        given()
                .body(MODEL)
                .contentType(ContentType.JSON)
                .when()
                .post(RESOURCE)
                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406)
                .body(MESSAGE_FIELD, containsString(Exceptions.ValorIncorrecto.getMensaje()));
    }

}
